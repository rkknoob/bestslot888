<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banner            = new Banner;
        $banner->sequence  = 1;
        $banner->name      = "สมัครสมาชิกใหม่รับโบนัส 30%";
        $banner->image     = "1.jpg";
        $banner->save();

        $banner            = new Banner;
        $banner->sequence  = 2;
        $banner->name      = "แจกเครดิตฟรี สูงสุด 12,000 บาท";
        $banner->image     = "2.jpg";
        $banner->save();

        $banner            = new Banner;
        $banner->sequence  = 3;
        $banner->name      = "ชวนเพื่อนรับโบนัสเพิ่ม 10%";
        $banner->image     = "3.jpg";
        $banner->save();

        $banner            = new Banner;
        $banner->sequence  = 4;
        $banner->name      = "โปรเสี่ยสั่งลุย ฝากทั้งวัน รับโบนัสเพิ่ม 25%";
        $banner->image     = "4.jpg";
        $banner->save();
    }
}
