<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where(['name' => "admin"])->first();
        $admin->syncPermissions(range(1, Permission::count()));

        $author = Role::where(['name' => "author"])->first();
        $author->syncPermissions([9, range(13, 24)]);
    }
}
