<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            // Role
            'role-list'   => 'ดู',
            'role-create' => 'เพิ่ม',
            'role-edit'   => 'แก้ไข',
            'role-delete' => 'ลบ',
            // User
            'user-list'   => 'ดู',
            'user-create' => 'เพิ่ม',
            'user-edit'   => 'แก้ไข',
            'user-delete' => 'ลบ',
            // Page
            'page-list'   => 'ดู',
            'page-create' => 'เพิ่ม',
            'page-edit'   => 'แก้ไข',
            'page-delete' => 'ลบ',
            // Banner
            'banner-list'   => 'ดู',
            'banner-create' => 'เพิ่ม',
            'banner-edit'   => 'แก้ไข',
            'banner-delete' => 'ลบ',
            // Promotion
            'promotion-list'   => 'ดู',
            'promotion-create' => 'เพิ่ม',
            'promotion-edit'   => 'แก้ไข',
            'promotion-delete' => 'ลบ',
            // Gold Number
            'gold-list'   => 'ดู',
            'gold-create' => 'เพิ่ม',
            'gold-edit'   => 'แก้ไข',
            'gold-delete' => 'ลบ',
        ];

        foreach ($permissions as $k => $permission) :
            Permission::create(['name' => $k, 'action' => $permission]);
        endforeach;
    }
}
