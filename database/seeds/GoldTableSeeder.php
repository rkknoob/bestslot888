<?php

use Illuminate\Database\Seeder;
use App\Models\Gold;

class GoldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gold            = new Gold;
        $gold->sequence  = 1;
        $gold->name      = "เขียว";
        $gold->condition = "500 ขึ้นไป";
        $gold->image     = "1.jpg";
        $gold->save();

        $gold            = new Gold;
        $gold->sequence  = 2;
        $gold->name      = "เหลือง";
        $gold->condition = "1,500 ขึ้นไป";
        $gold->image     = "2.jpg";
        $gold->save();

        $gold            = new Gold;
        $gold->sequence  = 3;
        $gold->name      = "แดง";
        $gold->condition = "2,000 ขึ้นไป";
        $gold->image     = "3.jpg";
        $gold->save();
    }
}
