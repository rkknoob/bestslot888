<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(BannerTableSeeder::class);
        $this->call(PromotionTableSeeder::class);
        $this->call(GoldTableSeeder::class);
    }
}
