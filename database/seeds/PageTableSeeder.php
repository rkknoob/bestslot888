<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page              = new Page;
        $page->sequence    = 1;
        $page->name        = "หน้าหลัก";
        $page->page        = "index";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 1;
        $page->save();

        $page              = new Page;
        $page->sequence    = 2;
        $page->name        = "สมัครสมาชิก";
        $page->page        = "register";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 1;
        $page->save();

        $page              = new Page;
        $page->sequence    = 3;
        $page->name        = "เข้าสู่ระบบ";
        $page->page        = "login";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 1;
        $page->save();

        $page              = new Page;
        $page->sequence    = 4;
        $page->name        = "เบอร์ทอง";
        $page->page        = "gold";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 0;
        $page->save();

        $page              = new Page;
        $page->sequence    = 5;
        $page->name        = "ดาวน์โหลด";
        $page->page        = "download";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 1;
        $page->save();

        $page              = new Page;
        $page->sequence    = 6;
        $page->name        = "โปรโมชั่น";
        $page->page        = "promotion";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 1;
        $page->save();

        $page              = new Page;
        $page->sequence    = 7;
        $page->name        = "ติดต่อเรา";
        $page->page        = "contact";
        $page->title       = "inwslot888 สล็อตออนไลน์ (slot online) เล่นบนมือถือ แจกเครดิตฟรี";
        $page->description = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->keywords    = "สล็อต, สล็อตออนไลน์, สล็อตxo, สล็อตออนไลน์ฟรีเครดิต , SLOTXO, 918KISS, LIVE22, JOKER123 , เกมส์สล็อตออนไลน์, เกมส์สล็อต";
        $page->active      = 1;
        $page->save();
    }
}
