<?php

use Illuminate\Database\Seeder;
use App\Models\Promotion;

class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promotion           = new Promotion;
        $promotion->sequence  = 1;
        $promotion->name      = "โปรฝากเงินนาทีทอง";
        $promotion->image     = "1.jpg";
        $promotion->save();

        $promotion            = new Promotion;
        $promotion->sequence  = 2;
        $promotion->name      = "ฝากวันแรกของวัน";
        $promotion->image     = "2.jpg";
        $promotion->save();

        $promotion            = new Promotion;
        $promotion->sequence  = 3;
        $promotion->name      = "ฝากทั้งวัน";
        $promotion->image     = "3.jpg";
        $promotion->save();

        $promotion            = new Promotion;
        $promotion->sequence  = 4;
        $promotion->name      = "เพื่อนชวนเพื่อน รับเครดิตฟรีทันที 25%";
        $promotion->image     = "4.jpg";
        $promotion->save();

        $promotion            = new Promotion;
        $promotion->sequence  = 5;
        $promotion->name      = "สมัครเพื่อเล่นเกม เครดิตฟรี 30%";
        $promotion->image     = "5.jpg";
        $promotion->save();

        $promotion            = new Promotion;
        $promotion->sequence  = 6;
        $promotion->name      = "โปรเสี่ยสั่งลุย ฝากทั้งวัน รับโบนัสเพิ่ม 25%";
        $promotion->image     = "6.jpg";
        $promotion->save();
    }
}
