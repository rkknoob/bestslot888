@extends('layouts.cms')
@section('title', '405 Page Permission')
@section('content')
<!-- Content area -->
<div class="content justify-content-center align-items-center mt-5">
    <!-- Container -->
    <div class="flex-fill"></div>
        <!-- Error title -->
        <div class="text-center mb-3">
        <h1 class="error-title">405</h1>
        <h5 class="font">  คุณไม่สิทธิ์ในการใช้งานหน้านี้  </h5>
        <!-- /error title -->
        <!-- Error content -->
        <br>
        <div class="row">
            <div class="col-xl-4 offset-xl-4 col-md-8 offset-md-2">
                <!-- Buttons -->
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ URL::previous() }}" class="btn btn-light btn-block mt-3 mt-sm-0"><i class="icon-arrow-left8 mr-2"></i> Back </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('dashboard') }}" class="btn btn-primary btn-block"><i class="icon-home4 mr-2"></i> Dashboard</a>
                    </div>
                </div>
                <!-- /buttons -->
            </div>
        </div>
        <!-- /error wrapper -->
    </div>
    <!-- /container -->
</div>
<!-- /content area -->
@endsection
