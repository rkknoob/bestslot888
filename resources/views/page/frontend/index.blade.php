@extends('layouts.app')

@section('title', title('index'))
@section("description", description('index'))
@section("keywords", keywords('index'))

@section('content')
<div class="container marketing">
    <div class="card">
        <div class="card-body">
            <div class="row featurette text-center">
                <div class="col-md-6 order-md-2">
                    <h2 class="featurette-heading"> สล็อตออนไลน์ Bestslot888 </span>
                    </h2>
                    <p class="lead mt-3">
                        สล็อต Bestslot888 เกมส์สล็อตออนไลน์ ไม่โกง <br>
                        ได้เท่าไรเราก็จ่าย เป็นเกมส์จาก slotxo สล็อตออนไลน์ ที่ดีที่สุดในเวลานี้เล่นง่าย เหมือนอยู่ใน casino สามารถเล่นสล็อตบนมือ
                        และมี เกมส์ยิงปลาบนมือถือ ด้วย ให้บริการฝาก-ถอน รวดเร็วทันใจตลอด 24 ชม. อีกทั้งยังมีแจกเครดิตฟรีทุกวัน
                    </p>
                    <a class="btn btn-dark" href="/register"> สมัครตอนนี้ </a>
                </div>
                <div class="col-md-6 order-md-1">
                    <img src="/images/joker.png" alt="" class="joker img-fluid mt-5">
                </div>
            </div>
        </div>
    </div>
    <hr class="featurette-divider">
    <div class="card">
        <div class="card-body">
            <div class="row">
                @php
                $name = [
                    1  => 'Tai Shang Lao Jun',
                    2  => 'Peach Banquet',
                    3  => "Third Prince's Journey",
                    4  => "Witch's Brew",
                    5  => 'Burning Pearl',
                    6  => 'Horus Eye',
                    7  => 'Lightning God',
                    8  => 'Lucky God Progressive',
                    9  => 'Robin Hood',
                    10 => 'Miami',
                    11 => 'Roma',
                    12 => 'Crazy Shark',
                    13 => 'King Derby',
                    14 => 'Five Dragons',
                    15 => 'Fish Hunter 2 Revo',
                    16 => 'Ancient Egypt',
                ];
                @endphp

                @for ($i = 1; $i <= 16; $i++)
                <div class="col-lg-3">
                    <div class="card text-center my-3">
                        <img src="/images/slot/{{$i}}.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h6 class="card-title"> {{ $name[$i] }} </h6>
                            <a href="#" class="btn btn-dark btn-sm"> Play Now </a>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
    <hr class="featurette-divider">
    <div class="card">
        <div class="card-body">
            <div class="row featurette text-center">
                <div class="col-md-7">
                    <h2 class="featurette-heading"> ศูนย์ดาวน์โหลด Bestslot888 </span></h2>
                    <p class="lead">
                        สำหรับท่านที่ต้องการเล่น Bestslot888
                        สล็อตออนไลน์ท่านสามารถดาวน์โหลด
                        ติดตั้งเกม InwSlot888 บนมือถือ โดยทำการสแกน Qr-code ที่ทางเรา ได้จัด
                        เตรียมไว้ให้ สามารถเล่น InwSlot888 ได้บนระบบ IOS และ Android ซึ่งท่าน
                        สามารถคลิกดูรายละเอียดการติดตั้งได้ที่ปุ่มด้านล่างได้ทันที หากต้องการ
                        สอบถามข้อมูลเพิ่มเติม สามารถติดต่อทีมงาน Bestslot888 ได้ตลอด 24 ช.ม.
                    </p>
                    <div class="row">
                        <div class="col-6 text-right">
                            <img src="/images/qr_code/android_title.png" alt="" class="mt-2 img-fluid">
                            <img src="/images/qr_code/android.png" alt="" class="mt-2 img-fluid">
                        </div>
                        <div class="col-6 text-left">
                            <img src="/images/qr_code/ios_title.png" alt="" class="mt-2 img-fluid">
                            <img src="/images/qr_code/ios.png" alt="" class="mt-2 img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <img src="/images/mobile.png" alt="" width="500" class="mobile img-fluid">
                </div>
            </div>
        </div>
    </div>
    <hr class="featurette-divider">
</div>
@endsection
