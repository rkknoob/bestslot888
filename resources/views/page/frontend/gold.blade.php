@extends('layouts.app')

@section('title', title('gold'))
@section("description", description('gold'))
@section("keywords", keywords('gold'))

@section('content')

<div class="container marketing">
    <div class="card pa-2 my-4">
        <div class="card-title text-center">
            <div class="card-body">
                <h2 class="mt-4"> เบอร์ทอง Bestslot888 </h2>
            </div>
        </div>
        <div class="card-body">
            <hr>
            <div class="row featurette text-center">
                @forelse (gold() as $rs)
                    <div class="col-md-12 my-4 pb-5" style="border-bottom: 1px solid #ccc;">
                        <h4> {{ $rs->name  }} </h4>
                        <p> {{ $rs->condition }}</p>
                        <img src="/images/gold/{{ $rs->image }}" alt="" class="promotion-image img-fluid" width="500">
                    </div>
                @empty
                    <div class="col-md-12 my-4 pb-5">
                        <p class="text-center"> ไม่มีข้อมูลเบอร์ทอง </p>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
    <hr class="featurette-divider">
</div>
@endsection
