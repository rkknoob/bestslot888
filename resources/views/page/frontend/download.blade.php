@extends('layouts.app')

@section('title', title('download'))
@section("description", description('download'))
@section("keywords", keywords('download'))

@section('content')
<div class="container marketing">
    <div class="card pa-2 my-4">
        <div class="card-title text-center">
            <h2 class="mt-4"> ดาวน์โหลด Bestslot888 </h2>
            <p>
                สำหรับท่านที่ต้องการเล่น Bestslot888 สล็อตออนไลน์ท่านสามารถดาวน์โหลด <br>
                ติดตั้งเกม Bestslot888 บนมือถือ โดยทำการสแกน Qr-code ที่ทางเรา ได้จัด เตรียมไว้ให้ <br>
                สามารถเล่น Bestslot888 ได้บนระบบ IOS และ Andriod ซึ่งท่าน สามารถคลิกดูรายละเอียดการติดตั้งได้ที่ปุ่มด้านล่างได้ทันที <br>
                หากต้องการ สอบถามข้อมูลเพิ่มเติม สามารถติดต่อทีมงาน Bestslot888 ได้ตลอด 24 ช.ม.
            </p>
        </div>
        <div class="card-body">
            <hr>
            <div class="row">
                <div class="col-3 text-center">
                    <img src="/images/qr_code/ios.png" alt="" class="mt-2 img-fluid">
                    <p>Please Scan QR Code</p>
                </div>
                <div class="col-3 text-center">
                    <h5 class="mt-5"> For Iphone IOS </h5>
                    <img src="/images/qr_code/ios_title.png" alt="" class="mt-2 img-fluid">
                </div>
                <div class="col-3 text-center">
                    <img src="/images/qr_code/android.png" alt="" class="mt-2 img-fluid">
                    <p>Please Scan QR Code</p>
                </div>
                <div class="col-3 text-center">
                    <h5 class="mt-5"> For Andriod Phone </h5>
                    <img src="/images/qr_code/android_title.png" alt="" class="mt-2 img-fluid">
                </div>
            </div>
        </div>
        <div class="card-body">
            <hr>
            <div class="row featurette">
                <div class="col-md-6 my-4">
                    <h3 class="text-center"> การติดตั้งเวอร์ชั่น iOS </h3>
                    <ul class="mt-4">
                        <li class="my-3">1. เมื่อกดปุ่ม “ดาวน์โหลดเวอร์ชั่น iOS” จากเว็บ MEGAWIN แล้ว ระบบจะเปิดหน้าเว็บของสล็อตเอ็กซ์โอ ให้คุณกดที่ปุ่ม “DOWNLOAD iOS” </li>
                        <img src="/images/download/ios1.png" alt="" class="mt-2 img-fluid">
                        <li class="my-3">2. จากนั้นให้กดปุ่ม “Install” จากนั้นระบบจะเริ่มทำการติดตั้งไฟล์แอพคาสิโนให้ทันที</li>
                        <li class="my-3">3. เมื่อติดตั้งแอพเสร็จแล้วจะยังไม่สามารถเล่นได้ในทันที จะต้องตั้งค่าเพื่อเพิ่มสิทธิ์ในการเข้าถึงแอพให้กับอุปกรณ์ iOS เสียก่อน โดยให้ไปที่ Setting (ตั้งค่า)</li>
                        <img src="/images/download/ios2.png" alt="" class="mt-2 img-fluid">
                        <li class="my-3">4. จากนั้นไปที่ “General (ทั่วไป)” >> “Profiles & Device Management (การจัดการอุปกรณ์)”</li>
                        <li class="my-3">5. จากนั้นเลือก “JTECH DEVELOPMENT PLT” แล้วให้กดปุ่ม “Trust (เชื่อถือ)” เมื่อคุณดำเนินการตั้งค่าในส่วนนี้เสร็จสิ้นแล้ว ให้กดที่ไอคอนแอพในหน้า Home บนมือถือคุณอีกครั้ง จะเห็นได้ว่าคุณสามารถเข้าใช้งานแอพคาสิโน MEGAWIN ได้ทันที ถ้าหากคุณลบแอพและติดตั้งใหม่ก็จะต้องทำการติดตั้งด้วยวิธีนี้อีกครั้ง</li>
                        <img src="/images/download/ios3.png" alt="" class="mt-2 img-fluid">
                    </ul>
                </div>
                <div class="col-md-6 my-4">
                    <h3 class="text-center"> การติดตั้งเวอร์ชั่น Android </h3>
                    <p class="my-3"> สำหรับผู้ที่ยังไม่ได้ดาวน์โหลดไฟล์สำหรับติดตั้งแอพเวอร์ชั่น Android กรุณากดดาวน์โหลดที่ปุ่มด้านล่างนี้เลย </p>
                    <ul class="mt-4">
                        <li class="my-3">1. เมื่อกดปุ่ม “ดาวน์โหลดเวอร์ชั่น Android” จากเว็บ MEGAWIN แล้ว ระบบจะเปิดหน้าเว็บของสล็อตเอ็กซ์โอ ให้คุณกดที่ปุ่ม “DOWNLOAD ANDROID”</li>
                        <li class="my-3">2. จากนั้นระบบจะแจ้งเตือนเพื่อทำการดาวน์โหลด ให้กดที่ปุ่ม “ตกลง”</li>
                        <li class="my-3">3.เมื่อดาวน์โหลดไฟล์ติดตั้งเสร็จแล้ว ไฟล์ที่ได้จะแสดงอยู่ในแถบแจ้งเตือนด้านบนของจอมือถือ ให้กดตรงนี้เพื่อเริ่มการติดตั้งแอพพลิเคชั่นได้เลย</li>
                        <li class="my-3">4. สำหรับบางท่านอาจจะไม่สามารถติดตั้งแอพได้ทันที ระบบจะสอบถามเพื่อขออนุญาตการติดตั้งเสียก่อน ฉะนั้นให้กดที่ปุ่ม “การตั้งค่า”</li>
                        <li class="my-3">5. หรือจะเข้าไปที่ “ตั้งค่า” >> “ความปลอดภัย” >> “ไม่รู้แหล่งที่มา” >> “อนุญาตให้ติดตั้งแอพพลิเคชั่นจากแหล่งที่ไม่รู้จัก” จากนั้นจึงจะสามารถติดตั้งแอพได้ จากนั้นรอระบบทำการติดตั้งสักครู่ แล้วจึงจะสามารถเล่นแอพคาสิโนสล็อตเอ็กซ์โอได้</li>
                        <img src="/images/download/howto.png" alt="" class="mt-2 img-fluid">
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr class="featurette-divider">
</div>
@endsection
