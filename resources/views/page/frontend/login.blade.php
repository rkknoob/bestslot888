<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ title('login') }} </title>
    <meta name="description" content="{{ description('login') }}">
    <meta name="keywords" content="{{ keywords('login') }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('template.frontend.header')
        <main role="main" class="mt-4">
            <section style="margin-top: 50px !important;">
                <iframe class="h_iframe" src="https://ambbo2.com/?prefix=Sk9F" frameborder="0" width="100%" height="700px"></iframe>
            </section>
            @include('template.frontend.footer')
        </main>
    </div>
</body>
</html>
