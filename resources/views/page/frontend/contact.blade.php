@extends('layouts.app')

@section('title', title('contact'))
@section("description", description('contact'))
@section("keywords", keywords('contact'))

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row featurette text-center">
                <div class="col-md-12">
                    <h2 class="featurette-heading"> ติดต่อเรา Bestslot888 </span> </h2>
                    <p class="lead mt-4">
                        สามารถติดต่อเรา Bestslot888 ได้  <br> หลายช่องทาง ทั้งไลน์แอด เฟสบุ๊ค และเบอร์โทร ท่านใดสนใจเล่น <br> Bestslot888 สมัครกันได้เลยตามภาพด้านล่าง
                    </p>
                    <a href="https://lin.ee/XRY6al">
                        <img src="/images/line.png" alt="" width="250" class="my-4 img-fluid">
                    </a>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <hr class="featurette-divider">
</div>
@endsection
