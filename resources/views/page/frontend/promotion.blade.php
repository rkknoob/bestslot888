@extends('layouts.app')

@section('title', title('promotion'))
@section("description", description('promotion'))
@section("keywords", keywords('promotion'))

@section('content')

<div class="container marketing">
    <div class="card pa-2 my-4">
        <div class="card-title text-center">
            <div class="card-body">
            <h2 class="mt-4"> โปรโมชั่น Bestslot888 </h2>
            <p>
                สำหรับสมาชิกสล็อตที่ต้องการโปรโมชั่นพิเศษจากเราสามารถดูรายละเอียดจาก โปรโมชั่นพิเศษ Bestslot888  <br>
                ที่ทางเราได้จัดเตรียมเอาไว้พิเศษสำหรับผู้เล่นต้องการเครดิตพิเศษเพิ่ม <br>
                โดยทำตามเงื่อนไขที่ทางเรากำหนดเอาไว้ก็สามารถถอนเงินได้ทันที
            </p>
        </div>
        </div>
        <div class="card-body">
            <hr>
            <div class="row featurette text-center">
                @forelse (promotion() as $rs)
                <div class="col-md-6 my-4">
                    <img src="/images/promotion/{{ $rs->image }}" alt="{{ $rs->name }}" class="promotion-image img-fluid" width="500">
                </div>
                @empty
                    <div class="col-md-12 my-4 pb-5">
                        <p class="text-center"> ไม่มีข้อมูลโปรโมชั่น </p>
                    </div>
                @endforelse
            </div>
            <hr>
        </div>
    </div>
    <hr class="featurette-divider">
</div>
@endsection
