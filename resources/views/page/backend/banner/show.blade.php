@extends('layouts.cms')
@section('title', 'รายละเอียดแบนเนอร์')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-images2', 'name' => 'รายละเอียดแบนเนอร์'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดแบนเนอร์
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> ชื่อ </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $banner->name }} </span>
                    </div>
                </div>

                <div class="card card-body bg-light mt-3">
                    <dl class="row mb-0">
                        <dt class="col col-sm-12 font text-center">
                            <strong> รูปภาพ </strong>
                        </dt>
                        <dd class="col-sm-12 mt-3 pa-2">
                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <div class="card">
                                        <div class="card-img-actions m-1">
                                            <img class="card-img img-fluid" src="/images/banner/{{ $banner->image }}" alt="">
                                            <div class="card-img-actions-overlay card-img">
                                                <a href="/images/banner/{{ $banner->image }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-fancybox="images">
                                                    <i class="icon-enlarge7"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
