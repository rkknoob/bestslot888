@extends('layouts.cms')
@section('title', 'เพิ่มแบนเนอร์')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-images2', 'name' => 'เพิ่มแบนเนอร์'])
<div class="content">
    <form action="{{ route('banner.store') }}" method="POST" id="FormValidation" enctype="multipart/form-data">
        @csrf
        <div class="card">
            <div class="card-header header-elesments-inline">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดแบนเนอร์
                    <span class="float-right">
                        <img src="/images/gold/no_image.png" width="100" height="60" class="rounded" id="photo">
                    </span>
                </legend>
            </div>
            <div class="card-body">
                <fieldset class="mb-2">
                    <div class="form-group row">
                        <label for="name" class="col-form-label col-lg-2 font">ชื่อ</label>
                        <div class="col-lg-10">
                            <input type="text" name="name" class="form-control" placeholder="ชื่อ">
                            @if ($errors->has('name'))
                                <span class="form-text text-danger"> {{ $errors->first('name') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-form-label col-lg-2 font"> รูปภาพ </label>
                        <div class="col-lg-10">
                            <input class="form-control" type="file" name="image" id="ActiveImage">
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@push('scripts')
    <script>
        function readImage(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#photo').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            }
        }

        $("#ActiveImage").change(function() {
            readImage(this);
        });

        document.addEventListener('DOMContentLoaded', function(e) {
            const form = document.getElementById('FormValidation');
            const fv = FormValidation.formValidation(
            form,
                {
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกชื่อ'
                                }
                            }
                        },
                        image: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณาเลือกรูปภาพ'
                                },
                                file: {
                                    extension: 'jpeg,jpg,png',
                                    type: 'image/jpeg,image/png',
                                    message: 'กรุณาเลือกไฟล์รูปภาพ'
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'icon-checkmark2',
                            invalid: 'icon-cross3',
                            validating: 'icon-spinner9'
                        }),
                    },
                }
            );
        });
    </script>
@endpush


