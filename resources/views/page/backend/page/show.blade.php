@extends('layouts.cms')
@section('title', 'รายละเอียดหน้า')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-list-numbered', 'name' => 'รายละเอียดหน้า'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดหน้า
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> ชื่อ </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $page->name }} </span>
                        <h6 class="font-weight-semibold mb-0 font f-22 mt-2"> <strong> หน้า </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $page->page }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mt-3">
                    <dl class="row mb-0">
                        <dt class="col col-sm-12 font text-center">
                            <strong> Title </strong>
                        </dt>
                        <dd class="col-sm-12 mt-3 pa-2">
                            <div class="row">
                                <div class="col-md-4 offset-md-4 text-center">
                                    <p>{!! $page->title !!}</p>
                                </div>
                            </div>
                        </dd>
                    </dl>
                    <br>
                    <dl class="row mb-0">
                        <dt class="col col-sm-12 font text-center">
                            <strong> Description </strong>
                        </dt>
                        <dd class="col-sm-12 mt-3 pa-2">
                            <div class="row">
                                <div class="col-md-4 offset-md-4 text-center">
                                    <p>{!! $page->description !!}</p>
                                </div>
                            </div>
                        </dd>
                    </dl>
                    <br>
                    <dl class="row mb-0">
                        <dt class="col col-sm-12 font text-center">
                            <strong> Keywords </strong>
                        </dt>
                        <dd class="col-sm-12 mt-3 pa-2">
                            <div class="row">
                                <div class="col-md-4 offset-md-4 text-center">
                                    <p>{!! $page->keywords !!}</p>
                                </div>
                            </div>
                        </dd>
                    </dl>
                    <br>
                    <dl class="row mb-0">
                        <dt class="col col-sm-12 font text-center">
                            <strong> Active </strong>
                        </dt>
                        <dd class="col-sm-12 mt-3 pa-2">
                            <div class="row">
                                <div class="col-md-4 offset-md-4 text-center">
                                    @switch($page->active)
                                        @case(0)
                                        <span class="badge bg-danger">ไม่แสดง</span>
                                        @break
                                        @case(1)
                                        <span class="badge bg-success">แสดง</span>
                                        @break
                                    @endswitch
                                </div>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
