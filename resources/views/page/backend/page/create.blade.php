@extends('layouts.cms')
@section('title', 'เพิ่มข้อมูลหน้า')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-stack-text', 'name' => 'เพิ่มข้อมูลหน้า'])
<div class="content">
    <form action="{{ route('page.store') }}" method="POST" id="FormValidation">
        @csrf
        <div class="card">
            <div class="card-header header-elesments-inline">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดข้อมูลหน้า
                </legend>
            </div>
            <div class="card-body">
                <fieldset class="mb-2">
                    <div class="form-group row">
                        <label for="name" class="col-form-label col-lg-2 font">ชื่อ</label>
                        <div class="col-lg-10">
                            <input type="text" name="name" class="form-control" placeholder="ชื่อ">
                            @if ($errors->has('name'))
                                <span class="form-text text-danger"> {{ $errors->first('name') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="page" class="col-form-label col-lg-2 font">หน้า</label>
                        <div class="col-lg-10">
                            <input type="text" name="page" class="form-control" placeholder="หน้า">
                            @if ($errors->has('page'))
                                <span class="form-text text-danger"> {{ $errors->first('page') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="page" class="col-form-label col-lg-2 font">สถานะ</label>
                        <div class="col-lg-10">
                            <div class="form-check form-check-inline mb-4">
                                <label class="form-check-label">
                                    <input type="radio" name="active" class="form-check-input-styled-primary" value="1">
                                    Active
                                </label>
                            </div>
                            <div class="form-check form-check-inline mb-4">
                                <label class="form-check-label">
                                    <input type="radio" name="active" class="form-check-input-styled-primary" value="0" checked>
                                    InActive
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="page" class="col-form-label col-lg-2 font"> Title </label>
                        <div class="col-lg-10">
                            <input type="text" name="title" class="form-control" placeholder="Title">
                            @if ($errors->has('title'))
                                <span class="form-text text-danger"> {{ $errors->first('title') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="page" class="col-form-label col-lg-2 font"> Description </label>
                        <div class="col-lg-10">
                            <textarea name="description" cols="30" rows="10" placeholder="Description" class="form-control"></textarea>
                            @if ($errors->has('description'))
                                <span class="form-text text-danger"> {{ $errors->first('description') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="page" class="col-form-label col-lg-2 font"> Keywords </label>
                        <div class="col-lg-10">
                            <textarea name="keywords" cols="30" rows="10" placeholder="Keywords" class="form-control"></textarea>
                            @if ($errors->has('keywords'))
                                <span class="form-text text-danger"> {{ $errors->first('keywords') }} </span>
                            @endif
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('template/plugins/uniform/uniform.min.js') }}"></script>
    <script>
        $('.form-check-input-styled-primary').uniform({
            wrapperClass: 'border-primary-600 text-primary-800'
        });
        document.addEventListener('DOMContentLoaded', function(e) {
            const form = document.getElementById('FormValidation');
            const fv = FormValidation.formValidation(
            form,
                {
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกชื่อ'
                                }
                            }
                        },
                        page: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกหน้า'
                                }
                            }
                        },
                        title: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอก title'
                                }
                            }
                        },
                        description: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอก description'
                                }
                            }
                        },
                        keywords: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอก keywords'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'icon-checkmark2',
                            invalid: 'icon-cross3',
                            validating: 'icon-spinner9'
                        }),
                    },
                }
            );
        });
    </script>
@endpush


