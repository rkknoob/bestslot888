@extends('layouts.cms')
@section('title', 'Page | ข้อมูลหน้า')
@section('content')
@include('template.backend.header',['icon'=> 'icon-stack-text', 'name' => 'ข้อมูลหน้า'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลหน้า </h5>
            @can('page-create')
            <a href="{{ route('page.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-plus2 mr-2"></i> เพิ่มข้อมูลหน้า
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-table">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">#</th>
                            <th>ลำดับ</th>
                            <th>ชื่อ</th>
                            <th>หน้า</th>
                            <th>สถานะ</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $k => $rs)
                        <tr>
                            <td class="text-center">{{ ++$k }}</td>
                            <td class="text-center" width="20%">
                                @include('template.backend.pattern.sequence', ['name' => 'page', 'sequence' => $rs->sequence, 'min' => $min, 'max' => $max])
                             </td>
                            <td class="text-left">{{ $rs->name }}</td>
                            <td class="text-left">{{ $rs->page }}</td>
                            <td class="text-center">
                                @switch($rs->active)
                                    @case(0)
                                    <span class="badge bg-danger">ไม่แสดง</span>
                                    @break
                                    @case(1)
                                    <span class="badge bg-success">แสดง</span>
                                    @break
                                @endswitch
                            </td>
                            <td class="text-center">
                                @include('template.backend.pattern.action', ['name' => 'page', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$('.datatable-table').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [4]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});
$('.btn-del').on('click',function(){
    let id = $(this).data('id');
    swal({
        title: "Are you sure?",
        text: "ต้องการที่จะลบ ข้อมูลหน้า นี้ใช่หรือไม่ !!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $( "#delete-"+id ).submit();
        }
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.up').on('click',function(){
    let no   = $(this).data('no');
    let name = $(this).data('name');
    $.ajax({
        type: "PATCH",
        url: "/backend/sequence/up",
        data: {no : no, name : name},
        success: function(rs){
            swal("Success!", "ทำการเลื่อนตำแหน่งเรียบร้อยแล้ว", "success", {button:false});
            setTimeout(function() {
                location.reload();
            }, 1200);
		}
	});
});

$('.down').on('click',function(){
    let no   = $(this).data('no');
    let name = $(this).data('name');
    $.ajax({
        type: "PATCH",
        url: "/backend/sequence/down",
        data: {no : no, name : name},
        success: function(rs){
            swal("Success!", "ทำการเลื่อนตำแหน่งเรียบร้อยแล้ว", "success",{button:false});
            setTimeout(function() {
                location.reload();
            }, 1200);
		}
	});
});
</script>
@endpush
