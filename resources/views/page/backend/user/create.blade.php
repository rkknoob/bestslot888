@extends('layouts.cms')
@section('title', 'เพิ่มผู้ใช้งาน')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-user-tie', 'name' => 'เพิ่มผู้ใช้งาน'])
<div class="content">
    <form action="{{ route('user.store') }}" method="POST" id="FormValidation">
        @csrf
        <div class="card">
            <div class="card-header header-elesments-inline">
                <legend class="text-uppercase font-size-sm font-weight-bold font f-20"> รายละเอียดผู้ใช้งาน </legend>
            </div>
            <div class="card-body">
                <fieldset class="mb-2">
                    <div class="form-group row">
                        <label for="name" class="col-form-label col-lg-2 font">ชื่อ</label>
                        <div class="col-lg-10">
                            <input type="text" name="name" class="form-control" placeholder="ชื่อ">
                            @if ($errors->has('name'))
                                <span class="form-text text-danger"> {{ $errors->first('name') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="username" class="col-form-label col-lg-2 font">ชื่อเข้าสู่ระบบ</label>
                        <div class="col-lg-10">
                            <input type="text" name="username" class="form-control" placeholder="ชื่อเข้าสู่ระบบ">
                            @if ($errors->has('username'))
                                <span class="form-text text-danger"> {{ $errors->first('username') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-form-label col-lg-2 font">รหัสผ่าน</label>
                        <div class="col-lg-10">
                            <input class="form-control" placeholder="รหัสผ่าน" type="password" name="password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password_confirmation" class="col-form-label col-lg-2 font">ยืนยันรหัสผ่าน</label>
                        <div class="col-lg-10">
                            <input class="form-control" placeholder="ยืนยันรหัสผ่าน" type="password" name="password_confirmation">
                            @if ($errors->has('password'))
                            <span class="form-text text-danger"> {{ $errors->first('password') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-form-label col-lg-2 font">อีเมล์</label>
                        <div class="col-lg-10">
                            <input type="email" name="email" class="form-control" placeholder="อีเมล์">
                            @if ($errors->has('email'))
                                <span class="form-text text-danger"> {{ $errors->first('email') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="roles" class="col-form-label col-lg-2 font">สิทธิ์ผู้ใช้งาน</label>
                        <div class="col-lg-10">
                            <select name="roles[]" class="form-control" multiple>
                                @foreach ($roles as $rs)
                                    <option value="{{ $rs }}"> {{ $rs }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('roles'))
                                <span class="form-text text-danger"> {{ $errors->first('roles') }} </span>
                            @endif
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function(e) {
            const form = document.getElementById('FormValidation');
            const fv = FormValidation.formValidation(
            form,
                {
                    fields: {
                        username: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกชื่อผู้ใช้งาน'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: 'กรุณากรอกตัวอักษรระหว่าง 6 ถึง 30 ตัวอักษร'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกพาสเวิร์ด'
                                },
                            }
                        },
                        password_confirmation: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกพาสเวิร์ดยืนยัน'
                                },
                                identical: {
                                    compare: function() {
                                        return form.querySelector('[name="password"]').value;
                                    },
                                    message: 'พาสเวิร์ดยืนยันไม่ตรงกัน'
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณากรอกอีเมล์'
                                },
                            }
                        },
                        roles: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณาเลือกสิทธิ์ผู้ใช้งาน'
                                },
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'icon-checkmark2',
                            invalid: 'icon-cross3',
                            validating: 'icon-spinner9'
                        }),
                    },
                }
            );
        });
    </script>
@endpush


