@extends('layouts.cms')
@section('title', 'User | ผู้ใช้งาน')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-user-tie', 'name' => 'ผู้ใช้งาน'])
<!-- Content area -->
<div class="content">
    <!-- Hover rows -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font f-22"> ผู้ใช้งาน </h5>
            @can('user-create')
            <a href="{{ route('user.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-user-plus mr-2"></i> เพิ่มผู้ใช้งาน
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-user">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">#</th>
                            <th>ชื่อ</th>
                            <th>ชื่อเข้าใช้งาน</th>
                            <th>อีเมล์</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $k => $rs)
                        <tr>
                            <td class="text-center">{{ ++$k }}</td>
                            <td>{{ $rs->name }}</td>
                            <td>{{ $rs->username }}</td>
                            <td>{{ $rs->email }}</td>
                            <td class="text-center">
                                @include('template.backend.pattern.action', ['name' => 'user', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
    <!-- /hover rows -->
</div>
<!-- /content area -->
@endsection

@push('scripts')
<script>
$('.datatable-user').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [4]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.btn-del').on('click',function(){
    let id = $(this).data('id');
    swal({
        title: "Are you sure?",
        text: "ต้องการที่จะลบ ผู้ใช้งาน นี้ใช่หรือไม่ !!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $( "#delete-"+id ).submit();
        }
    });
});
</script>
@endpush
