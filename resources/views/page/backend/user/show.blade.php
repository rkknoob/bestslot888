@extends('layouts.cms')
@section('title', 'ข้อมูลผู้ใช้ระบบ')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-user-tie','name' => 'ข้อมูลผู้ใช้ระบบ'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22"> รายละเอียดผู้ใช้ระบบ </h6>
                <p class="mb-3 text-muted"> User Description. </p>
                <div class="card card-body bg-light mb-0">
                    <dl class="row mb-0">
                        <dt class="col-lg-2 col-xs-5 font"> <b> ชื่อ </b>  </dt>
                        <dd class="col-lg-4 col-xs-7"> {{ $user->name }}</dd>

                        <dt class="col-lg-2 col-xs-5 font"> <b> ชื่อเข้าใช้ระบบ </b>  </dt>
                        <dd class="col-lg-4 col-xs-7"> {{ $user->username }}</dd>

                        <dt class="col-lg-2 col-xs-5 font"> <b> อีเมล์ </b>  </dt>
                        <dd class="col-lg-4 col-xs-7"> {{ $user->email }}</dd>

                        <dt class="col-lg-2 col-xs-5 font"> <b> สิทธิ์การเข้าใช้งาน </b>  </dt>
                        <dd class="col-lg-4 col-xs-7">
                            <p class="text-muted">
                                @if(!empty($user->getRoleNames()))
                                  @foreach($user->getRoleNames() as $v)
                                      <label class="badge badge-success"> {{ $v }} </label>
                                  @endforeach
                                @endif
                              </p>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


