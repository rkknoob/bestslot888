@extends('layouts.cms')
@section('title', 'ข้อมูลสิทธิ์การใช้งาน')
@section('content')
@include('template.backend.header', ['icon'=>'icon-collaboration', 'name' => 'ข้อมูลสิทธิ์การใช้งาน'])
<!-- Content area -->
<div class="content">
    <div class="card">
        <div class="card-header">
            <legend class="font-size-sm font-weight-bold font f-20"> ข้อมูลสิทธิ์การใช้งาน <span class="f-20 float-right"> {{ $role->name }} </span> </legend>
        </div>
        <div class="card-body">
            <p class="text-muted">
                <div class="row">
                    <div class="col-md-6">
                        @if(!empty($rolePermissions))
                            @foreach($rolePermissions as $v)
                                @foreach(role() as $k => $rs)
                                    @if($v->name == $k)
                                    <p class="font role" style="margin:12px 0px 12px 0px">  <span style="margin-left:10px">{{ $rs }}</span>  </p>
                                    @endif
                                @endforeach
                                    @switch($v->action)
                                        @case("ดู")
                                        <span class="badge bg-primary font" style="font-size:18px;margin:0px 20px; font-weight: normal;padding:10px 15px"> {{ $v->action }} </span>
                                        @break
                                        @case("เพิ่ม")
                                        <span class="badge bg-green font" style="font-size:18px;margin:0px 20px; font-weight: normal;padding:10px 15px"> {{ $v->action }} </span>
                                        @break
                                        @case("แก้ไข")
                                        <span class="badge bg-warning font" style="font-size:18px;margin:0px 20px; font-weight: normal;padding:10px 15px"> {{ $v->action }} </span>
                                        @break
                                        @case("ลบ")
                                        <span class="badge bg-danger font" style="font-size:18px;margin:0px 20px; font-weight: normal;padding:10px 15px"> {{ $v->action }} </span>
                                        @break
                                    @endswitch
                            @endforeach
                        @endif
                    </div>
                </div>
            </p>
            <hr>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
