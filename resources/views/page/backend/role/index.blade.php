@extends('layouts.cms')
@section('title', 'Role | สิทธิ์การใช้งาน')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-collaboration', 'name' => 'สิทธิ์การใช้งาน'])
<!-- Content area -->
<div class="content">
    <!-- Hover rows -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font f-22"> สิทธิ์การใช้งาน </h5>
            @can('role-create')
            <a href="{{ route('role.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-add mr-2"></i> เพิ่มสิทธิ์การใช้งาน
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                        <tr class="bg-slate-600">
                            <th width="10%" class="text-center">#</th>
                            <th width="50%">ประเภทผู้ใช้งาน</th>
                            <th width="40%">กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $K => $rs)
                        <tr>
                            <td class="text-center">{{ ++$K }}</td>
                            <td>{{ $rs->name }}</td>
                            <td>
                                <div class="list-icons">
                                    <a href="{{ route('role.show', $rs->id) }}" class="btn list-icons-item text-primary-600"><i class="icon-file-eye"></i></a>
                                    @can('role-edit')
                                    <a href="{{ route('role.edit', $rs->id) }}" class="btn list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                    @endcan
                                    @can('role-delete')
                                    <a class="btn btn-del list-icons-item" data-id="{{ $rs->id }}"><i class="icon-trash text-danger-600"></i></a>
                                    <form action="{{ route('role.destroy', $rs->id) }}" method="POST" id="delete-{{ $rs->id }}">
                                        @method('DELETE') @csrf
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /hover rows -->
</div>
<!-- /content area -->
@endsection
@push('scripts')
    <script>
        $('.btn-del').on('click',function(){
            let id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "ต้องการที่จะลบ สิทธิ์ผู้ใช้งาน นี้ใช่หรือไม่ !!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $( "#delete-"+id ).submit();
                }
            });
        });
    </script>
@endpush
