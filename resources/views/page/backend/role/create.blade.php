@extends('layouts.cms')
@section('title', 'เพิ่มสิทธิ์การใช้งาน')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-collaboration', 'name' => 'เพิ่มสิทธิ์การใช้งาน'])
<!-- Content area -->
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <legend class="text-uppercase font-size-sm font-weight-bold font f-20"> รายละเอียดสิทธิ์การใช้งาน </legend>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('role.store') }}">
                @csrf
                <fieldset class="mb-4">
                    <div class="form-group row">
                        <label for="name" class="col-form-label col-lg-2 font"> สิทธิ์การใช้งาน </label>
                        <div class="col-lg-10">
                            <input type="text" name="name" class="form-control">
                            @if ($errors->has('name'))
                                <span class="form-text text-danger font"> {{ $errors->first('name') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 font"> การอนุญาติเข้าใช้งาน </label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    @foreach($permission as $value)
                                       @foreach(role() as $k => $v)
                                            @if($value->name == $k)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="font f-20 mt-2 role"> <span class="ml-2">{{ $v }} </span> </p>
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                        <div class="form-check form-check-inline mb-4">
                                            <label class="form-check-label">
                                                <input type="checkbox" name="permission[]" class="form-check-input-styled-primary" value="{{ $value->id }}">
                                                {!! $value->action !!}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->
</div>
<!-- /content area -->
@endsection
@push('scripts')
    <script src="{{ asset('template/plugins/uniform/uniform.min.js') }}"></script>
    <script>
        $('.form-check-input-styled-primary').uniform({
            wrapperClass: 'border-primary-600 text-primary-800'
        });
    </script>
@endpush
