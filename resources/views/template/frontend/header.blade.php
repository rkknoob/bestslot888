<header>
    <div class="navbest fixed-top">
        <div class="row">
            <div class="col-md-3">
                <nav class="navbar navbar-expand-md navbar-dark">
                    <div class="container">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="navbar-brand navbar-logo mx-auto" href="#">
                            <img src="/images/logo.png" width="120" alt="" class="my-1">
                        </a>
                    </div>
                </nav>
            </div>
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-9">
                        <div class="container text-right">
                            <div class="boxed-wrapper">
                                <div id="member-login">
                                    <form method="post" action="#">
                                        <a class="button-submit" style="color: #000" href="https://www.superslot999.com/member/register"><i
                                                class="fas fa-pencil-alt" style="font-size: 0.8em;"></i> สมัครสมาชิก</a>
                                        <a class="button-submit" style="color: #000" href="https://www.superslot999.com/member/topup"><i class="fas fa-sign-in-alt" style="font-size: 0.8em;"></i> เข้าสู่ระบบ</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row navbar-expand-md mx-auto">
                    <div class="col-md-9">
                        <div class="container">
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                            <ul class="navbar-nav ml-auto n-dif-top text-center">
                                @foreach (page() as $item)
                                    <li class="nav-item {{ Str::startsWith(Request::route()->getName(), $item->page) ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route($item->page) }}"> {{ $item->name }} </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</header>


