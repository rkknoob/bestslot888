<div id="CarouselBanner" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach (banner() as $k => $item)
            @if($k == 0)
            <li data-target="#CarouselBanner" data-slide-to="{{ $k }}" class="active"></li>
            @else
            <li data-target="#CarouselBanner" data-slide-to="{{ $k }}"></li>
            @endif
        @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach (banner() as $k => $item)
            @if($k == 0)
            <div class="carousel-item active">
                <img src="/images/banner/{{ $item->image }}" class="d-block w-100 img-fluid img-banner" alt="{{ $item->name }}">
            </div>
            @else
            <div class="carousel-item">
                <img src="/images/banner/{{ $item->image }}" class="d-block w-100 img-fluid img-banner" alt="{{ $item->name }}">
            </div>
            @endif
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#CarouselBanner" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#CarouselBanner" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
