@if (session('success'))
    <script>
        swal("Success!", "บันทึกข้อมูลเรียบร้อยแล้ว", "success");
    </script>
@elseif (session('update'))
    <script>
        swal("Updated!", "แก้ไขข้อมูลเรียบร้อยแล้ว!", "success");
    </script>
@elseif (session('delete'))
    <script>
        swal("Deleted!", "ลบข้อมูลเรียบร้อยแล้ว!", "success");
    </script>
@endif
