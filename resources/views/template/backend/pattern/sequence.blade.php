@if($min == $sequence)
<button
    class="btn btn-outline bg-success border-success text-success-800 btn-icon down"
    data-name="{{ $name }}" data-no="{{ $sequence }}"
    data-popup="tooltip"
    title="เลื่อนตำแหน่งลง"
    data-placement="top"
    id="top"
    data-original-title="top tooltip">
    <i class="icon-arrow-down12"></i>
</button>
@elseif($min < $sequence && $sequence < $max)
<button
    class="btn btn-outline bg-success border-success text-success-800 btn-icon up"
    data-name="{{ $name }}" data-no="{{ $sequence }}"
    data-popup="tooltip"
    title="เลื่อนตำแหน่งขึ้น"
    data-placement="bottom"
    id="bottom"
    data-original-title="bottom tooltip">
    <i class="icon-arrow-up12"></i>
</button>
<button
    class="btn btn-outline bg-success border-success text-success-800 btn-icon down"
    data-name="{{ $name }}" data-no="{{ $sequence }}"
    data-popup="tooltip"
    title="เลื่อนตำแหน่งลง"
    data-placement="top"
    id="top"
    data-original-title="top tooltip">
    <i class="icon-arrow-down12"></i>
</button>
@else
<button
    class="btn btn-outline bg-success border-success text-success-800 btn-icon up"
    data-name="{{ $name }}" data-no="{{ $sequence }}"
    data-popup="tooltip"
    title="เลื่อนตำแหน่งขึ้น"
    data-placement="bottom"
    id="bottom"
    data-original-title="bottom tooltip">
    <i class="icon-arrow-up12"></i>
</button>
@endif
