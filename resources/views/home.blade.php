@extends('layouts.cms')
@section('title', 'Home | หน้าหลัก')
@section('content')
@include('template.backend.header', ['icon'=> 'icon-home2', 'name' => 'หน้าหลัก'])
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font"> Menu </h6>
                </div>
                <div class="card-body px-2">
                    <div class="box-body">
						<div class="row">
                            @foreach (menu() as $item)
                            @can($item['role'])
                            <div class="col-sm-6 col-md-3 animated fadeInRight">
								<div class="block text-center">
									<a href="{{ $item['route'] }}" class="hvr-pop">
										<div class="icon">
											<br>
                                            <i class="{{ $item['icon'] }}" style="font-size:30px;"></i>
											<br>
											<span class="font" style="font-size:20px"> {{ $item['name'] }} </span>
											<br>
											<br>
										</div>
									</a>
								</div>
                            </div>
                            @endcan
                            @endforeach
						</div>
					</div>
                </div>
            </div>
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font"> USER </h6>
                </div>
                <div class="card-body px-2">
                    <div class="box-body">
						<div class="row">
                            @foreach (user() as $item)
                            @can($item['role'])
                            <div class="col-sm-6 col-md-3 animated fadeInRight">
								<div class="block text-center">
									<a href="{{ $item['route'] }}" class="hvr-pop">
										<div class="icon">
											<br>
                                            <i class="{{ $item['icon'] }}" style="font-size:30px;"></i>
											<br>
											<span class="font" style="font-size:20px"> {{ $item['name'] }} </span>
											<br>
											<br>
										</div>
									</a>
								</div>
                            </div>
                            @endcan
                            @endforeach
						</div>
					</div>
                </div>
            </div>
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font"> PROFILE </h6>
                </div>
                <div class="card-body px-2">
                    <div class="box-body">
						<div class="row">
							<div class="col-sm-6 col-md-3 animated fadeInRight">
								<div class="block text-center">
									<a href="{{ route('profile.index', Auth::user()->id) }}" class="hvr-pop">
										<div class="icon">
											<br>
                                            <i class="icon-profile" style="font-size:30px;"></i>
											<br>
											<span class="font" style="font-size:20px"> โปรไฟล์ </span>
											<br>
											<br>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 animated fadeInRight">
								<div class="block text-center">
									<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-dashboard').submit();" class="hvr-pop">
                                        <form action="{{ route('logout') }}" id="logout-form-dashboard" method="POST">
                                            @csrf
                                        </form>
										<div class="icon">
											<br>
											<i class="icon-switch2" style="font-size:30px;"></i>
											<br>
											<span class="font" style="font-size:20px"> ออกจากระบบ </span>
											<br>
											<br>
										</div>
									</a>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
