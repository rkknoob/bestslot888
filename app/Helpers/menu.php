<?php

function menu()
{
    return [
        'page'      => ['submenu' => FALSE, 'route' => route('page.index')      , 'icon' => 'icon-stack-text'    , 'role' => 'page-list'      , 'name' => 'ข้อมูลหน้า'],
        'banner'    => ['submenu' => FALSE, 'route' => route('banner.index')    , 'icon' => 'icon-images2'       , 'role' => 'banner-list'    , 'name' => 'แบนเนอร์'],
        'promotion' => ['submenu' => FALSE, 'route' => route('promotion.index') , 'icon' => 'icon-gift'          , 'role' => 'promotion-list' , 'name' => 'โปรโมชั่น'],
        'gold'      => ['submenu' => FALSE, 'route' => route('gold.index')      , 'icon' => 'icon-list-numbered' , 'role' => 'gold-list'      , 'name' => 'เบอร์ทอง'],
    ];
}

function user()
{
    return [
        'role' => ['route' => route('role.index') , 'icon' => 'icon-collaboration' , 'role' => 'role-list' , 'submenu' => FALSE, 'name' => 'สิทธิ์การใช้งาน'],
        'user' => ['route' => route('user.index') , 'icon' => 'icon-users'         , 'role' => 'user-list' , 'submenu' => FALSE, 'name' => 'ผู้ใช้งาน'],
    ];
}
