<?php

use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Banner;
use App\Models\Promotion;
use App\Models\Gold;
use App\Models\Page;

function role()
{
    return [
        'role-list'      => 'สิทธิ์การใช้งาน',
        'user-list'      => 'ผู้ใช้งาน',
        'page-list'      => 'ข้อมูลหน้า',
        'banner-list'    => 'แบนเนอร์',
        'promotion-list' => 'โปรโมชั่น',
        'gold-list'      => 'เบอร์ทอง',
    ];
}

function uploadImage($image)
{
    $image['edit_image'] ? File::delete('images/'.$image['name'].'/'.$image['edit_image']) : NULL;

    $file = $image['image'];

    $name = Str::random(5).$file->getClientOriginalName();

    $image_resize = Image::make($file->getRealPath());

    $height = Image::make($image_resize)->height();
    $width  = Image::make($image_resize)->width();

    $image_resize->resize($width, $height);

    $image_resize->save('images/'.$image['name'].'/'.$name);

    return $name;
}

function banner()
{
    return Banner::orderBy('sequence')->get();
}

function promotion()
{
    return Promotion::orderBy('sequence')->get();
}

function gold()
{
    return Gold::orderBy('sequence')->get();
}

function title($page)
{
    $page = Page::where('page', $page)->first();
    return $page->title;
}

function description($page)
{
    $page = Page::where('page', $page)->first();
    return $page->description;
}

function keywords($page)
{
    $page = Page::where('page', $page)->first();
    return $page->keywords;
}

function page()
{
    return Page::where('active', 1)->orderBy('sequence')->get();
}
