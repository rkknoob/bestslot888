<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use File;

class BannerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:banner-list');
        $this->middleware('permission:banner-create', ['only' => ['create','store']]);
        $this->middleware('permission:banner-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:banner-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('page.backend.banner.index')
        ->withItems(Banner::orderBy('sequence')->get())
        ->withMax(Banner::max('sequence'))
        ->withMin(Banner::min('sequence'));
    }

    public function create()
    {
        return view('page.backend.banner.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'image' => 'mimes:jpeg,jpg,png'
        ]);

        $banner = new Banner;
        $banner->sequence  = Banner::max('sequence') + 1;
        $banner->name      = $request->name;
        $banner->image     = $request->hasFile('image') ? uploadImage(['name' => 'banner' ,'image' => $request->image, 'edit_image' => NULL]) : "no_image.png";
        $banner->save();
        return redirect()->route('banner.index')->with('success','Success');
    }

    public function show(Banner $banner)
    {
        return view('page.backend.banner.show')->withBanner($banner);
    }

    public function edit(Banner $banner)
    {
        return view('page.backend.banner.edit')->withBanner($banner);
    }

    public function update(Request $request, Banner $banner)
    {
        $request->validate([
            'name'      => 'required',
            'image'     => 'mimes:jpeg,jpg,png'
        ]);
        $banner->name   = $request->name;
        $banner->image  = $request->hasFile('image') ? uploadImage(['name' => 'banner', 'image' => $request->image, 'edit_image' => $request->edit_image]) : "no_image.png";
        $banner->save();
        return redirect()->route('banner.index')->with('success','Success');
    }

    public function destroy(Banner $banner)
    {
        File::delete('images/banner/'.$banner->image);
        Banner::destroy($banner->id);
        return redirect()->route('banner.index')->with('delete','Success');
    }
}
