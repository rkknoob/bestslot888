<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use Illuminate\Http\Request;
use File;

class PromotionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:promotion-list');
        $this->middleware('permission:promotion-create', ['only' => ['create','store']]);
        $this->middleware('permission:promotion-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:promotion-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('page.backend.promotion.index')
        ->withItems(Promotion::orderBy('sequence')->get())
        ->withMax(Promotion::max('sequence'))
        ->withMin(Promotion::min('sequence'));
    }

    public function create()
    {
        return view('page.backend.promotion.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'image' => 'mimes:jpeg,jpg,png'
        ]);
        $promotion = new Promotion;
        $promotion->sequence  = Promotion::max('sequence') + 1;
        $promotion->name      = $request->name;
        $promotion->image     = $request->hasFile('image') ? uploadImage(['name' => 'promotion' ,'image' => $request->image, 'edit_image' => NULL]) : "no_image.png";
        $promotion->save();
        return redirect()->route('promotion.index')->with('success','Success');
    }

    public function show(Promotion $promotion)
    {
        return view('page.backend.promotion.show')->withPromotion($promotion);
    }

    public function edit(Promotion $promotion)
    {
        return view('page.backend.promotion.edit')->withPromotion($promotion);
    }

    public function update(Request $request, Promotion $promotion)
    {
        $request->validate([
            'name'      => 'required',
            'image'     => 'mimes:jpeg,jpg,png'
        ]);
        $promotion->name   = $request->name;
        $promotion->image  = $request->hasFile('image') ? uploadImage(['name' => 'promotion', 'image' => $request->image, 'edit_image' => $request->edit_image]) : "no_image.png";
        $promotion->save();
        return redirect()->route('promotion.index')->with('success','Success');
    }

    public function destroy(Promotion $promotion)
    {
        File::delete('images/promotion/'.$promotion->image);
        Promotion::destroy($promotion->id);
        return redirect()->route('promotion.index')->with('delete','Success');
    }
}
