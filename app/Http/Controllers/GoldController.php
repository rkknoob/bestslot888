<?php

namespace App\Http\Controllers;

use App\Models\Gold;
use Illuminate\Http\Request;
use File;

class GoldController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:gold-list');
        $this->middleware('permission:gold-create', ['only' => ['create','store']]);
        $this->middleware('permission:gold-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:gold-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('page.backend.gold.index')
        ->withItems(Gold::orderBy('sequence')->get())
        ->withMax(Gold::max('sequence'))
        ->withMin(Gold::min('sequence'));
    }

    public function create()
    {
        return view('page.backend.gold.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required',
            'condition' => 'required',
            'image'     => 'mimes:jpeg,jpg,png'
        ]);

        $gold = new Gold;
        $gold->sequence  = Gold::max('sequence') + 1;
        $gold->name      = $request->name;
        $gold->condition = $request->condition;
        $gold->image     = $request->hasFile('image') ? uploadImage(['name' => 'gold', 'image' => $request->image, 'edit_image' => NULL]) : "no_image.png";
        $gold->save();
        return redirect()->route('gold.index')->with('success','Success');
    }

    public function show(Gold $gold)
    {
        return view('page.backend.gold.show')->withGold($gold);
    }

    public function edit(Gold $gold)
    {
        return view('page.backend.gold.edit')->withGold($gold);
    }

    public function update(Request $request, Gold $gold)
    {
        $request->validate([
            'name'      => 'required',
            'condition' => 'required',
            'image'     => 'mimes:jpeg,jpg,png'
        ]);
        $gold->name      = $request->name;
        $gold->condition = $request->condition;
        $gold->image     = $request->hasFile('image') ? uploadImage(['name' => 'gold', 'image' => $request->image, 'edit_image' => $request->edit_image]) : "no_image.png";
        $gold->save();
        return redirect()->route('gold.index')->with('success','Success');
    }

    public function destroy(Gold $gold)
    {
        File::delete('images/gold/'.$gold->image);
        Gold::destroy($gold->id);
        return redirect()->route('gold.index')->with('delete','Success');
    }
}
