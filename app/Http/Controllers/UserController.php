<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list');
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $user = User::all();
        return view('page.backend.user.index')->withItems($user);
    }

    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('page.backend.user.create')->with(['roles' => $roles]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'     => 'required',
            'username' => 'required|unique:users',
            'email'    => 'required|email|unique:users,email',
            'password' => 'confirmed',
            'roles'    => 'required'
        ]);

        $user           = new User;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;
        $user->password = $request->password;
        $user->save();
        $user->assignRole($request->input('roles'));

        return redirect()->route('user.index')->with('success','Success');
    }

    public function show(User $user)
    {
        return view('page.backend.user.show')->withUser($user);
    }

    public function edit(User $user)
    {
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        return view('page.backend.user.edit')->with(['user' => $user, 'roles' => $roles, 'userRole' => $userRole]);
    }

    public function update(Request $request, User $user)
    {
        $message = [
            'username.required'  => 'กรุณากรอกชื่อผู้ใช้งาน',
            'username.unique'    => 'มีชื่อผู้ใช้งานในระบบแล้ว',
            'password.confirmed' => 'รหัสยืนยันไม่ตรงกัน',
            'email.required'     => 'กรุณากรอกอีเมล์',
            'email.unique'       => 'มีผู้ใช้อีเมล์นี้แล้ว',
        ];

        $request->validate([
            'name'     => 'required',
            'username' => 'required|min:6|max:30|unique:users,username,'.$user->id,
            'password' => 'confirmed',
            'email'    => 'required|email|unique:users,email,'.$user->id,
        ], $message);

        $user->name       = $request->name;
        $user->username   = $request->username;
        $user->password   = $request->password ? $request->password : $user->password;
        $user->email      = $request->email;
        $user->save();
        DB::table('model_has_roles')->where('model_id',$user->id)->delete();
        $user->assignRole($request->input('roles'));

        return redirect()->route('user.index')->with('update','Success');
    }

    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->route('user.index')->with('delete','Successfully');
    }
}
