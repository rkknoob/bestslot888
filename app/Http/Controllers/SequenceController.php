<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Banner;
use App\Models\Promotion;
use App\Models\Gold;

class SequenceController extends Controller
{
    public function up(Request $request)
    {
        switch($request->name):
            case "page":
                $sequence_a = Page::where('sequence',   $request->no)->first();
                $sequence_b = Page::where('sequence', --$request->no)->first();
            break;
            case "banner":
                $sequence_a = Banner::where('sequence',   $request->no)->first();
                $sequence_b = Banner::where('sequence', --$request->no)->first();
            break;
            case "promotion":
                $sequence_a = Promotion::where('sequence',   $request->no)->first();
                $sequence_b = Promotion::where('sequence', --$request->no)->first();
            break;
            case "gold":
                $sequence_a = Gold::where('sequence',   $request->no)->first();
                $sequence_b = Gold::where('sequence', --$request->no)->first();
            break;
        endswitch;

        $sequence_a->sequence = --$sequence_a->sequence;
        $sequence_b->sequence = ++$sequence_b->sequence;
        $sequence_b->save();
        $sequence_a->save();

        return response("success up");
    }

    public function down(Request $request)
    {
        switch($request->name):
            case "page":
                $sequence_a = Page::where('sequence',   $request->no)->first();
                $sequence_b = Page::where('sequence', --$request->no)->first();
            break;
            case "banner":
                $sequence_a = Banner::where('sequence',   $request->no)->first();
                $sequence_b = Banner::where('sequence', ++$request->no)->first();
            break;
            case "promotion":
                $sequence_a = Promotion::where('sequence',   $request->no)->first();
                $sequence_b = Promotion::where('sequence', ++$request->no)->first();
            break;
            case "gold":
                $sequence_a = Gold::where('sequence',   $request->no)->first();
                $sequence_b = Gold::where('sequence', ++$request->no)->first();
            break;
        endswitch;

        $sequence_a->sequence = ++$sequence_a->sequence;
        $sequence_b->sequence = --$sequence_b->sequence;
        $sequence_b->save();
        $sequence_a->save();

        return response("success down");
    }
}
