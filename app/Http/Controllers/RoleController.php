<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $roles = Role::get();
        return view('page.backend.role.index',compact('roles'));
    }

    public function create()
    {
        $permission = Permission::get();
        return view('page.backend.role.create',compact('permission'));
    }

    public function store(Request $request)
    {
        $message = [
            'name.unique'   => 'มีชื่อสิทธิ์การใช้งานนี้ในระบบแล้ว',
            'name.required' => 'กรุณากรอกสิทธิ์การใช้งาน'
        ];
        $request->validate(['name' => 'required|unique:roles'], $message);
        $role = Role::create(['name' => $request->name]);
        $role->syncPermissions($request->permission);
        return redirect()->route('role.index')->with('success','Success');
    }

    public function show(Role $role)
    {
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id","permissions.id")
        ->where("role_has_permissions.role_id",$role->id)->get();
        return view('page.backend.role.show',compact('role','rolePermissions'));
    }

    public function edit(Role $role)
    {
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")
        ->where("role_has_permissions.role_id",$role->id)
        ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
        ->all();
        return view('page.backend.role.edit', compact('role', 'permission', 'rolePermissions'));
    }

    public function update(Request $request, Role $role)
    {
        $message = ['name.unique'  => 'มีชื่อสิทธิ์การใช้งานนี้ในระบบแล้ว'];
        $request->validate(['name' => 'required|unique:roles,name,'.$role->id], $message);
        $role->name = $request->name;
        $role->save();
        $role->syncPermissions($request->permission);
        return redirect()->route('role.index')->with('update','Updated Successfully');
    }

    public function destroy($id)
    {
        Role::destroy($id);
        return redirect()->route('role.index')->with('delete','Success');
    }
}
