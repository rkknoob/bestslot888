<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:page-list');
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('page.backend.page.index')
        ->withItems(Page::orderBy('sequence')->get())
        ->withMax(Page::max('sequence'))
        ->withMin(Page::min('sequence'));
    }

    public function create()
    {
        return view('page.backend.page.create');
    }

    public function store(Request $request)
    {
        $page = new Page;
        $page->sequence    = Page::max('sequence') + 1;
        $page->name        = $request->name;
        $page->page        = $request->page;
        $page->title       = $request->title;
        $page->description = $request->description;
        $page->keywords    = $request->keywords;
        $page->active      = $request->active;
        $page->save();
        return redirect()->route('page.index')->with('success','Success');
    }

    public function show(Page $page)
    {
        return view('page.backend.page.show')->withPage($page);
    }

    public function edit(Page $page)
    {
        return view('page.backend.page.edit')->withPage($page);
    }

    public function update(Request $request, Page $page)
    {
        $page->name        = $request->name;
        $page->page        = $request->page;
        $page->title       = $request->title;
        $page->description = $request->description;
        $page->keywords    = $request->keywords;
        $page->active      = $request->active;
        $page->save();
        return redirect()->route('page.index')->with('success','Success');
    }

    public function destroy(Page $page)
    {
        Page::destroy($page->id);
        return redirect()->route('page.index')->with('delete','Success');
    }
}
