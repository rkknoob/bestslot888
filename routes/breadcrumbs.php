<?php

// Home
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('หน้าหลัก', route('dashboard'));
});

// Home > Profile
Breadcrumbs::for('profile.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('โปรไฟล์', route('profile.index'));
});
Breadcrumbs::register('profile.edit', function ($trail) {
    $trail->parent('profile.index');
    $trail->push('แก้ไขโปรไฟล์', route('profile.edit'));
});

// Role
Breadcrumbs::register('role.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('สิทธิ์การใช้งาน', route('role.index'));
});
Breadcrumbs::register('role.create', function ($trail) {
    $trail->parent('role.index');
    $trail->push('เพิ่มสิทธิ์การใช้งาน', route('role.create'));
});
Breadcrumbs::register('role.edit', function ($trail, $id) {
    $trail->parent('role.index');
    $trail->push('แก้ไขสิทธิ์การใช้งาน', route('role.edit', $id));
});
Breadcrumbs::register('role.show', function ($trail, $id) {
    $trail->parent('role.index');
    $trail->push('ข้อมูลสิทธิ์การใช้งาน', route('role.show', $id));
});

// User
Breadcrumbs::register('user.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('ผู้ใช้งาน', route('user.index'));
});
Breadcrumbs::register('user.create', function ($trail) {
    $trail->parent('user.index');
    $trail->push('เพิ่มผู้ใช้งาน', route('user.create'));
});
Breadcrumbs::register('user.edit', function ($trail, $id) {
    $trail->parent('user.index');
    $trail->push('แก้ไขผู้ใช้งาน', route('user.edit', $id));
});
Breadcrumbs::register('user.show', function ($trail, $id) {
    $trail->parent('user.index');
    $trail->push('ข้อมูลผู้ใช้งาน', route('user.show', $id));
});

// Page
Breadcrumbs::register('page.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('ข้อมูลหน้า', route('page.index'));
});
Breadcrumbs::register('page.create', function ($trail) {
    $trail->parent('page.index');
    $trail->push('เพิ่มข้อมูลหน้า', route('page.create'));
});
Breadcrumbs::register('page.edit', function ($trail, $id) {
    $trail->parent('page.index');
    $trail->push('แก้ไขข้อมูลหน้า', route('page.edit', $id));
});
Breadcrumbs::register('page.show', function ($trail, $id) {
    $trail->parent('page.index');
    $trail->push('รายละเอียดข้อมูลหน้า', route('page.show', $id));
});


// Banner
Breadcrumbs::register('banner.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('แบนเนอร์', route('banner.index'));
});
Breadcrumbs::register('banner.create', function ($trail) {
    $trail->parent('banner.index');
    $trail->push('เพิ่มแบนเนอร์', route('banner.create'));
});
Breadcrumbs::register('banner.edit', function ($trail, $id) {
    $trail->parent('banner.index');
    $trail->push('แก้ไขแบนเนอร์', route('banner.edit', $id));
});
Breadcrumbs::register('banner.show', function ($trail, $id) {
    $trail->parent('banner.index');
    $trail->push('ข้อมูลแบนเนอร์', route('banner.show', $id));
});

// Promotion
Breadcrumbs::register('promotion.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('โปรโมชั่น', route('promotion.index'));
});
Breadcrumbs::register('promotion.create', function ($trail) {
    $trail->parent('promotion.index');
    $trail->push('เพิ่มโปรโมชั่น', route('promotion.create'));
});
Breadcrumbs::register('promotion.edit', function ($trail, $id) {
    $trail->parent('promotion.index');
    $trail->push('แก้ไขโปรโมชั่น', route('promotion.edit', $id));
});
Breadcrumbs::register('promotion.show', function ($trail, $id) {
    $trail->parent('promotion.index');
    $trail->push('ข้อมูลโปรโมชั่น', route('promotion.show', $id));
});

// Gold
Breadcrumbs::register('gold.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('เบอร์ทอง', route('gold.index'));
});
Breadcrumbs::register('gold.create', function ($trail) {
    $trail->parent('gold.index');
    $trail->push('เพิ่มเบอร์ทอง', route('gold.create'));
});
Breadcrumbs::register('gold.edit', function ($trail, $id) {
    $trail->parent('gold.index');
    $trail->push('แก้ไขเบอร์ทอง', route('gold.edit', $id));
});
Breadcrumbs::register('gold.show', function ($trail, $id) {
    $trail->parent('gold.index');
    $trail->push('ข้อมูลเบอร์ทอง', route('gold.show', $id));
});

