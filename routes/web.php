<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/'         , 'page/frontend/index')->name('index');
Route::view('register'  , 'page/frontend/register')->name('register');
Route::view('login'     , 'page/frontend/login')->name('login');
Route::view('gold'      , 'page/frontend/gold')->name('gold');
Route::view('download'  , 'page/frontend/download')->name('download');
Route::view('promotion' , 'page/frontend/promotion')->name('promotion');
Route::view('contact'   , 'page/frontend/contact')->name('contact');

Route::namespace('Auth')->prefix('backend')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login-backend');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::middleware('auth')->prefix('backend')->group(function () {
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::prefix('profile')->group(function () {
        Route::GET('/', 'ProfileController@index')->name('profile.index');
        Route::GET('edit', 'ProfileController@edit')->name('profile.edit');
        Route::PUT('update', 'ProfileController@update')->name('profile.update');
    });

    Route::PATCH('sequence/up',   'SequenceController@up');
    Route::PATCH('sequence/down', 'SequenceController@down');

    Route::resource('role'      , 'RoleController');
    Route::resource('user'      , 'UserController');
    Route::resource('page'      , 'PageController');
    Route::resource('banner'    , 'BannerController');
    Route::resource('promotion' , 'PromotionController');
    Route::resource('gold'      , 'GoldController');
});
